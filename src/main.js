/**
 * Extend the default Array object with a method to shuffle the elements in the array.
 * Optionally the array can be partially shuffled.
 * @param {Number} beginIndex - The index at which shuffling should begin (inclusive).
 * @param {Number} endIndex - The index at which shuffling should end (exclusive).
 * @return {Array} - A new array with its elements shuffled.
 */
Array.prototype.shuffle = function (beginIndex = 0, endIndex = this.length) {
    // Define a prefix and a postfix for the parts of the array that should not be shuffled.
    const arrayPrefix = this.slice(0, beginIndex);
    const array = this.slice(beginIndex, endIndex);
    const arrayPostfix = this.slice(Math.max(beginIndex, endIndex));
    // Shuffle the array.
    if (array.length > 1) {
        const initialArray = [...array];
        while (array.toString() === initialArray.toString()) {
            // Fisher-Yates shuffle algorithm.
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
        }
    }
    return arrayPrefix.concat(array).concat(arrayPostfix);
}


class Sentence {
    constructor(sentence) {
        this.toString = () => {
            return sentence.toString();
        }

        this.toArray = () => {
            return sentence.toString().trim().split(' ').filter(el => el !== '');
        }
    }

    difference(sentence) {
        const words = new Sentence(sentence.toString()).toArray();
        return new Sentence(this.toArray().filter(word => !words.includes(word)).join(' '));
    }

    mix(keepHeadAndTail = false) {
        const beginIndex = keepHeadAndTail ? 1 : undefined;
        const endIndex = keepHeadAndTail ? this.toArray().length - 1 : undefined;
        return new Sentence(this.toArray().shuffle(beginIndex, endIndex).join(' '));
    }

    // TODO
    score() {
        return 'Niet geïmplementeerd.'
    }
}


// Listen to input in the 'mix' form.
document.forms['mix'].addEventListener('input', (event) => {
    const mixForm = event.currentTarget;
    const sentence = new Sentence(mixForm.input.value);
    mixForm.output.value = sentence.mix(mixForm.keep_head_and_tail.checked).toString();
});


// Listen to input in the 'search' form.
document.forms['search'].addEventListener('input', (event) => {
    const searchForm = event.currentTarget;
    searchForm.missing_words.value = new Sentence(searchForm.exercise.value).difference(searchForm.answer.value).toString();
    searchForm.remaining_words.value = new Sentence(searchForm.answer.value).difference(searchForm.exercise.value).toString()
});


// Listen to input in the 'score' form.
document.forms['score'].addEventListener('input', (event) => {
    const scoreForm = event.currentTarget;
    const exercise = new Sentence(scoreForm.exercise.value);
    scoreForm.score.value = exercise.score(scoreForm.answer.value);
});
